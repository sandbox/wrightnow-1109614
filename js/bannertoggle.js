$(document).ready(function(){
    //_authcache_course_timer_setCookies(" . $bookID . "," . $required . "," . $spent . "," . time() . ");

    var cookie = $.cookie('bannertoggle')

    if (cookie != null && cookie.length > 0){
        var target = $.cookie('bannertarget');
        var newSiteName = $.cookie('bannername');
        var newLogo =$.cookie('bannerlogo');
        var newSlogan = $.cookie('bannerslogan');
        var blurb = $.cookie('bannerblurb');        

        // change all home links to the new target.
        //$("a[href='/']").attr('href', target);
        $("a[title='Home']").attr('href', target);

        $("#logo-title").attr('class', cookie);
        
        if (newSiteName != null && newSiteName.length >0){
            var site2 = newSiteName.replace(/\+/g, " ");
            var printSite ="<a href='" + target + "' title='home' rel='home'>"+ site2 + "</a>";            
            $('#site-name').html(printSite);
        }
        
        if (newLogo != null && newLogo.length >0){
            var logo2 = newLogo.replace(/\+/g, " ");            
            var printlogo = "<a  href='" + target + "' title='home' rel='home'>\n\
            <img src='/"+ logo2+"' alt='Home' id='logo-image'></img></a>";
            $('#logo').html(printlogo);
        }

        var fullblurb = null;
        if(blurb != null && blurb.length >0){
            var blurb2 = blurb.replace(/\+/g, " ");
            fullblurb = "<span id='bannertoggleblurb'>" + blurb2 + "</span>";
        }

        if (newSlogan != null && newSlogan.length >0 ){
            var slogan2 = newSlogan.replace(/\+/g, " ");
            var printslogan = "<a href='" + target + "' id='siteslogan' title='home' rel='home'>"+ slogan2 + "</a>";
            if (fullblurb != null){
                printslogan = "<a href='" + target + "' id='siteslogan' title='home' rel='home'>"+ slogan2 +"<br/>" + fullblurb +  "</a>";
            }
            $('#site-slogan').html(printslogan);
        }else if (fullblurb != null){
            $('#site-slogan').html(fullblurb);
        }

        if (bannertoggle_testIsIE()){            
            $('#siteslogan').css('margin-left', '0px'); 
            $('#site-slogan').css('margin-left', '0px');
        }
    }
});

function bannertoggle_testIsIE(){
    var isIE =  false;
    jQuery.each(jQuery.browser, function(i, val) {
        if(i=="msie" && val){
            isIE=true;
        }
    });
    return isIE;

}