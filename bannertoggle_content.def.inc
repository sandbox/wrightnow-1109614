<?php

function _bannertoggle_object_export() {
    $content['type'] = array(
        'name' => 'Bannertoggle',
        'type' => 'bannertoggle',
        'description' => 'Create an entity for the banner toggle system.',
        'title_label' => 'Title',
        'body_label' => '',
        'min_word_count' => '0',
        'help' => '',
        'node_options' =>
        array(
            'status' => true,
            'promote' => false,
            'sticky' => false,
            'revision' => false,
        ),
        'upload' => '0',
        'search_block' => '1',
        'old_type' => 'bannertoggle',
        'orig_type' => '',
        'module' => 'node',
        'custom' => '1',
        'modified' => '1',
        'locked' => '0',
        'image_attach' => '0',
        'image_attach_maximum' => '0',
        'image_attach_size_teaser' => 'thumbnail',
        'image_attach_size_body' => 'thumbnail',
        'og_content_type_usage' => 'omitted',
        'og_max_groups' => '',
        'nodewords_edit_metatags' => 0,
        'nodewords_metatags_generation_method' => '0',
        'nodewords_metatags_generation_source' => '2',
        'nodewords_use_alt_attribute' => 1,
        'nodewords_filter_modules_output' =>
        array(
            'imagebrowser' => false,
            'img_assist' => false,
        ),
        'nodewords_filter_regexp' => '',
        'comment' => '0',
        'comment_default_mode' => '4',
        'comment_default_order' => '1',
        'comment_default_per_page' => '50',
        'comment_controls' => '3',
        'comment_anonymous' => 0,
        'comment_subject_field' => '1',
        'comment_preview' => '1',
        'comment_form_location' => '0',
        'fivestar' => 0,
        'fivestar_stars' => 5,
        'fivestar_labels_enable' => 1,
        'fivestar_label_0' => 'Cancel rating',
        'fivestar_label_1' => 'Poor',
        'fivestar_label_2' => 'Okay',
        'fivestar_label_3' => 'Good',
        'fivestar_label_4' => 'Great',
        'fivestar_label_5' => 'Awesome',
        'fivestar_label_6' => 'Give it @star/@count',
        'fivestar_label_7' => 'Give it @star/@count',
        'fivestar_label_8' => 'Give it @star/@count',
        'fivestar_label_9' => 'Give it @star/@count',
        'fivestar_label_10' => 'Give it @star/@count',
        'fivestar_style' => 'average',
        'fivestar_text' => 'dual',
        'fivestar_title' => 1,
        'fivestar_feedback' => 1,
        'fivestar_unvote' => 0,
        'fivestar_position_teaser' => 'hidden',
        'fivestar_position' => 'below',
        'fivestar_comment' => 0,
        'print_display' => 0,
        'print_display_comment' => 0,
        'print_display_urllist' => 0,
        'print_mail_display' => 0,
        'print_mail_display_comment' => 0,
        'print_mail_display_urllist' => 0,
        'print_pdf_display' => 0,
        'print_pdf_display_comment' => 0,
        'print_pdf_display_urllist' => 0,
    );
    $content['fields'] = array(
        0 =>
        array(
            'label' => 'Site Name',
            'field_name' => 'field_bannertoggle_sitename',
            'type' => 'text',
            'widget_type' => 'text_textfield',
            'change' => 'Change basic information',
            'weight' => '-4',
            'rows' => 5,
            'size' => '60',
            'description' => 'Display a name for the site',
            'default_value' =>
            array(
                0 =>
                array(
                    'value' => '',
                    '_error_element' => 'default_value_widget][field_bannertoggle_sitename][0][value',
                ),
            ),
            'default_value_php' => '',
            'default_value_widget' => NULL,
            'group' => false,
            'required' => 1,
            'multiple' => '0',
            'text_processing' => '0',
            'max_length' => '',
            'allowed_values' => '',
            'allowed_values_php' => '',
            'op' => 'Save field settings',
            'module' => 'text',
            'widget_module' => 'text',
            'columns' =>
            array(
                'value' =>
                array(
                    'type' => 'text',
                    'size' => 'big',
                    'not null' => false,
                    'sortable' => true,
                    'views' => true,
                ),
            ),
            'display_settings' =>
            array(
                'label' =>
                array(
                    'format' => 'above',
                    'exclude' => 0,
                ),
                5 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'teaser' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'full' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                4 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                2 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                3 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'token' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
            ),
        ),
        1 =>
        array(
            'label' => 'Slogan',
            'field_name' => 'field_bannertoggle_slogan',
            'type' => 'text',
            'widget_type' => 'text_textfield',
            'change' => 'Change basic information',
            'weight' => '-3',
            'rows' => 5,
            'size' => '100',
            'description' => 'Short description of the site',
            'default_value' =>
            array(
                0 =>
                array(
                    'value' => '',
                    '_error_element' => 'default_value_widget][field_bannertoggle_slogan][0][value',
                ),
            ),
            'default_value_php' => '',
            'default_value_widget' =>
            array(
                'field_bannertoggle_slogan' =>
                array(
                    0 =>
                    array(
                        'value' => '',
                        '_error_element' => 'default_value_widget][field_bannertoggle_slogan][0][value',
                    ),
                ),
            ),
            'group' => false,
            'required' => 1,
            'multiple' => '0',
            'text_processing' => '0',
            'max_length' => '',
            'allowed_values' => '',
            'allowed_values_php' => '',
            'op' => 'Save field settings',
            'module' => 'text',
            'widget_module' => 'text',
            'columns' =>
            array(
                'value' =>
                array(
                    'type' => 'text',
                    'size' => 'big',
                    'not null' => false,
                    'sortable' => true,
                    'views' => true,
                ),
            ),
            'display_settings' =>
            array(
                'label' =>
                array(
                    'format' => 'above',
                    'exclude' => 0,
                ),
                5 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'teaser' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'full' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                4 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                2 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                3 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'token' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
            ),
        ),
        2 =>
        array(
            'label' => 'Target',
            'field_name' => 'field_bannertoggle_target',
            'type' => 'text',
            'widget_type' => 'text_textfield',
            'change' => 'Change basic information',
            'weight' => '-2',
            'rows' => 5,
            'size' => '60',
            'description' => 'Where to target the banner\'s link?',
            'default_value' =>
            array(
                0 =>
                array(
                    'value' => '',
                    '_error_element' => 'default_value_widget][field_bannertoggle_target][0][value',
                ),
            ),
            'default_value_php' => '',
            'default_value_widget' =>
            array(
                'field_bannertoggle_target' =>
                array(
                    0 =>
                    array(
                        'value' => '',
                        '_error_element' => 'default_value_widget][field_bannertoggle_target][0][value',
                    ),
                ),
            ),
            'group' => false,
            'required' => 1,
            'multiple' => '0',
            'text_processing' => '0',
            'max_length' => '',
            'allowed_values' => '',
            'allowed_values_php' => '',
            'op' => 'Save field settings',
            'module' => 'text',
            'widget_module' => 'text',
            'columns' =>
            array(
                'value' =>
                array(
                    'type' => 'text',
                    'size' => 'big',
                    'not null' => false,
                    'sortable' => true,
                    'views' => true,
                ),
            ),
            'display_settings' =>
            array(
                'label' =>
                array(
                    'format' => 'above',
                    'exclude' => 0,
                ),
                5 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'teaser' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'full' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                4 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                2 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                3 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'token' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
            ),
        ),
        3 =>
        array(
            'label' => 'Blurb',
            'field_name' => 'field_bannertoggle_blurb',
            'type' => 'text',
            'widget_type' => 'text_textfield',
            'change' => 'Change basic information',
            'weight' => '-1',
            'rows' => 5,
            'size' => '60',
            'description' => 'Longer description of the site.',
            'default_value' =>
            array(
                0 =>
                array(
                    'value' => '',
                    '_error_element' => 'default_value_widget][field_bannertoggle_blurb][0][value',
                ),
            ),
            'default_value_php' => '',
            'default_value_widget' =>
            array(
                'field_bannertoggle_blurb' =>
                array(
                    0 =>
                    array(
                        'value' => '',
                        '_error_element' => 'default_value_widget][field_bannertoggle_blurb][0][value',
                    ),
                ),
            ),
            'group' => false,
            'required' => 0,
            'multiple' => '0',
            'text_processing' => '0',
            'max_length' => '',
            'allowed_values' => '',
            'allowed_values_php' => '',
            'op' => 'Save field settings',
            'module' => 'text',
            'widget_module' => 'text',
            'columns' =>
            array(
                'value' =>
                array(
                    'type' => 'text',
                    'size' => 'big',
                    'not null' => false,
                    'sortable' => true,
                    'views' => true,
                ),
            ),
            'display_settings' =>
            array(
                'label' =>
                array(
                    'format' => 'above',
                    'exclude' => 0,
                ),
                5 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'teaser' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'full' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                4 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                2 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                3 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'token' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
            ),
        ),
        4 =>
        array(
            'label' => 'Triggers',
            'field_name' => 'field_bannertoggle_triggers',
            'type' => 'nodereference',
            'widget_type' => 'nodereference_autocomplete',
            'change' => 'Change basic information',
            'weight' => 0,
            'autocomplete_match' => 'contains',
            'size' => '60',
            'description' => 'Select nodes that will trigger this banner.',
            'default_value' =>
            array(
                0 =>
                array(
                    'nid' => NULL,
                    '_error_element' => 'default_value_widget][field_bannertoggle_triggers][0][nid][nid',
                ),
            ),
            'default_value_php' => '',
            'default_value_widget' =>
            array(
                'field_bannertoggle_triggers' =>
                array(
                    0 =>
                    array(
                        'nid' =>
                        array(
                            'nid' => '',
                            '_error_element' => 'default_value_widget][field_bannertoggle_triggers][0][nid][nid',
                        ),
                        '_error_element' => 'default_value_widget][field_bannertoggle_triggers][0][nid][nid',
                    ),
                ),
            ),
            'group' => false,
            'required' => 1,
            'multiple' => '1',
            'referenceable_types' =>
            array(
                'book' => 'book',
                'coursepage' => 'coursepage',
                'group_setup' => 'group_setup',
                'page' => 'page',
                'product' => 'product',
                'program' => 'program',
                'webform' => 'webform',
                'bannertoggle' => 0,
                'blog' => 0,
                'coa' => 0,
                'spi' => 0,
                'course_credit_type' => 0,
                'course_didactic_page' => 0,
                'website' => 0,
                'faq' => 0,
                'forum' => 0,
                'image' => 0,
                'internal_resource' => 0,
                'long_answer' => 0,
                'site_blog' => 0,
                'matching' => 0,
                'multichoice' => 0,
                'standardized_patient' => 0,
                'patient_rater_form' => 0,
                'poll' => 0,
                'product_kit' => 0,
                'program_credit_type' => 0,
                'quiz' => 0,
                'quiz_directions' => 0,
                'references' => 0,
                'scale' => 0,
                'school_page' => 0,
                'short_answer' => 0,
                'story' => 0,
                'truefalse' => 0,
            ),
            'advanced_view' => '--',
            'advanced_view_args' => '',
            'op' => 'Save field settings',
            'module' => 'nodereference',
            'widget_module' => 'nodereference',
            'columns' =>
            array(
                'nid' =>
                array(
                    'type' => 'int',
                    'unsigned' => true,
                    'not null' => false,
                    'index' => true,
                ),
            ),
            'display_settings' =>
            array(
                'label' =>
                array(
                    'format' => 'above',
                    'exclude' => 0,
                ),
                5 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'teaser' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'full' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                4 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                2 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                3 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'token' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
            ),
        ),
        5 =>
        array(
            'label' => 'Groups',
            'field_name' => 'field_bannertoggle_groups',
            'type' => 'nodereference',
            'widget_type' => 'nodereference_select',
            'change' => 'Change basic information',
            'weight' => '1',
            'autocomplete_match' => 'contains',
            'size' => 60,
            'description' => 'Select groups that will trigger the banner.',
            'default_value' =>
            array(
                0 =>
                array(
                    'nid' => '',
                ),
            ),
            'default_value_php' => '',
            'default_value_widget' =>
            array(
                'field_bannertoggle_groups' =>
                array(
                    0 =>
                    array(
                        'nid' => '',
                    ),
                    'nid' =>
                    array(
                        'nid' =>
                        array(
                            0 => '',
                        ),
                    ),
                ),
            ),
            'group' => false,
            'required' => 0,
            'multiple' => '1',
            'referenceable_types' =>
            array(
                'group_setup' => 'group_setup',
                'bannertoggle' => 0,
                'blog' => 0,
                'book' => 0,
                'coa' => 0,
                'spi' => 0,
                'course_credit_type' => 0,
                'course_didactic_page' => 0,
                'coursepage' => 0,
                'website' => 0,
                'faq' => 0,
                'forum' => 0,
                'image' => 0,
                'internal_resource' => 0,
                'long_answer' => 0,
                'site_blog' => 0,
                'matching' => 0,
                'multichoice' => 0,
                'standardized_patient' => 0,
                'page' => 0,
                'patient_rater_form' => 0,
                'poll' => 0,
                'product' => 0,
                'product_kit' => 0,
                'program' => 0,
                'program_credit_type' => 0,
                'quiz' => 0,
                'quiz_directions' => 0,
                'references' => 0,
                'scale' => 0,
                'school_page' => 0,
                'short_answer' => 0,
                'story' => 0,
                'truefalse' => 0,
                'webform' => 0,
            ),
            'advanced_view' => '--',
            'advanced_view_args' => '',
            'op' => 'Save field settings',
            'module' => 'nodereference',
            'widget_module' => 'nodereference',
            'columns' =>
            array(
                'nid' =>
                array(
                    'type' => 'int',
                    'unsigned' => true,
                    'not null' => false,
                    'index' => true,
                ),
            ),
            'display_settings' =>
            array(
                'label' =>
                array(
                    'format' => 'above',
                    'exclude' => 0,
                ),
                5 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'teaser' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'full' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                4 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                2 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                3 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'token' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
            ),
        ),
        6 =>
        array(
            'label' => 'Logo',
            'field_name' => 'field_bannertoggle_logo',
            'type' => 'text',
            'widget_type' => 'text_textfield',
            'change' => 'Change basic information',
            'weight' => '2',
            'rows' => 5,
            'size' => '150',
            'description' => 'Select an image to use as a logo',
            'default_value' =>
            array(
                0 =>
                array(
                    'value' => '',
                    '_error_element' => 'default_value_widget][field_bannertoggle_logo][0][value',
                ),
            ),
            'default_value_php' => '',
            'default_value_widget' =>
            array(
                'field_bannertoggle_logo' =>
                array(
                    0 =>
                    array(
                        'value' => '',
                        '_error_element' => 'default_value_widget][field_bannertoggle_logo][0][value',
                    ),
                ),
            ),
            'group' => false,
            'required' => 0,
            'multiple' => '0',
            'text_processing' => '0',
            'max_length' => '',
            'allowed_values' => '',
            'allowed_values_php' => '',
            'op' => 'Save field settings',
            'module' => 'text',
            'widget_module' => 'text',
            'columns' =>
            array(
                'value' =>
                array(
                    'type' => 'text',
                    'size' => 'big',
                    'not null' => false,
                    'sortable' => true,
                    'views' => true,
                ),
            ),
            'display_settings' =>
            array(
                'label' =>
                array(
                    'format' => 'above',
                    'exclude' => 0,
                ),
                5 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'teaser' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'full' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                4 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                2 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                3 =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
                'token' =>
                array(
                    'format' => 'default',
                    'exclude' => 0,
                ),
            ),
        ),
    );
    $content['extra'] = array(
        'title' => '-5',
        'revision_information' => '8',
        'author' => '7',
        'options' => '9',
        'comment_settings' => '12',
        'menu' => '3',
        'book' => '6',
        'path' => '10',
        'detailed_question' => '5',
        'print' => '11',
        'nodewords' => '4',
    );

    return $content;
}